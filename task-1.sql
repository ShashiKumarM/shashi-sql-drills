
CREATE TABLE IF NOT EXISTS branch (

	branch_id INT PRIMARY KEY NOT NULL, 
	branch_addr VARCHAR(255), 
	ISBN VARCHAR(50) NOT NULL, 
	title VARCHAR(100), 
	author VARCHAR(100), 
	publisher VARCHAR(100), 
	num_copies INT DEFAULT 0
);
