
CREATE TABLE IF NOT EXISTS `client`(
	
	client_id INT PRIMARY KEY NOT NULL,
	client_name VARCHAR(100),
	location VARCHAR(255),
	manager_id INT NOT NULL,
	manager_location VARCHAR(255),
	contract_id INT NOT NULL, 
	estimated_cost INT,
	completion_date DATE,
	staff_id INT NOT NULL, 
	staff_name VARCHAR(100),
	staff_location VARCHAR(255)
);
