
CREATE TABLE IF NOT EXISTS patient(

	patient_id INT PRIMARY KEY NOT NULL, 
	patient_name VARCHAR(100),
	DOB DATE,
	address VARCHAR(255),
	prescription VARCHAR(255),
	drug VARCHAR(100),
	date_of_visit DATE,
	dosage VARCHAR(100),
	doctor_id INT, 
	FOREIGN KEY(doctor_id) REFERENCES doctor(doctor_id)
);
